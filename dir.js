require('dotenv').config()
let Client = require('ssh2-sftp-client');

let sftp = new Client();
sftp.connect({
    host: process.env.FTP_HOST,
    // port: '8080',
    username: process.env.FTP_USER,
    password: process.env.FTP_PASSWORD
}).then(() => {
    return sftp.list('.');
}).then((data) => {
    console.log(data, 'the data info');
}).catch((err) => {
    console.log(err, 'catch error');
});
