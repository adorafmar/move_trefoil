require 'net/sftp'
require 'aws-sdk-s3'
require '/home/ubuntu/uem/move_trefoil/config.rb'

def object_uploaded?(s3_client, bucket_name, object_key, body)
  response = s3_client.put_object(
    bucket: bucket_name,
    key: object_key,
    body: body
  )
  if response.etag
    return true
  else
    return false
  end 
rescue StandardError => e
  puts Time.now.inspect + " Error uploading object: #{e.message}"
  return false
end

found = false
Net::SFTP.start(secret(:FTP_HOST), secret(:FTP_USER), :password => secret(:FTP_PASSWORD)) do |sftp|
  sftp.dir.foreach(".") do |entry|
    if entry.name.match("Dashboard_.*[.]csv") then
      found = true
      sftp.download! entry.name, entry.name

      bucket_name = 'utpower'
      object_key = 'Trefoil/raw/'+entry.name
      region = 'us-east-1'
      s3_client = Aws::S3::Client.new(region: region)

      if object_uploaded?(s3_client, bucket_name, object_key, File.open(entry.name))
        puts Time.now.inspect + " " + "Object '#{object_key}' uploaded to bucket '#{bucket_name}'"
      else
        puts Time.now.inspect + " " + "Object '#{object_key}' failed to upload to bucket '#{bucket_name}'"
      end 
    end 
    # sftp.remove entry.name
  end 
end

if not found 
  puts Time.now.inspect + " Nothing found"
end
